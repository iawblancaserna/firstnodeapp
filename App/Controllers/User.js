// // ponemos exports porque sino no se puede usar la funcion fuera de este controller
var isMayor = (req, res, next) => {

    if (req.body.age >= 18) {
        res.send("Major d'edat");
    } else {
        res.send("Menor d'edat");
    }
};

//controllers/user.js

var User = require('../../Models/user');

//Create a new user and save it
var add = (req, res)=>{
    var user = new User({name: req.body.name, age: req.body.age});
    user.save();
    console.log(user);
    return user;
};

//find all people
var list = (req, res, next)=>{
    User.find(function(err, users) {
        return users;
    });
};

//find person by id
var find = (req, res)=>{
    User.findOne({_id: req.params.id}, function(error, user) {
        return user;
    })
};

module.exports = {
add,
list,
find,
isMayor
}