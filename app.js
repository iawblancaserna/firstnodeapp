var express = require('express');

var app = express();
var  mongoose = require('mongoose');
  var http = require('http');
/*
app.get('/', function (req, res) { // req --> $request - res --> $respuesta
  res.send('Hello World!');
});
*/
const index = require('./routes/index');
const path = __dirname + '/views/';

app.set('view engine', 'html');  // per saber quina vista agafes i de quin tipus es
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path));

app.use('/', index);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

//Load app dependencies

mongoose.connect(
  `mongodb://root:pass12345@localhost:27017/tutorial?authSource=admin`,
  {useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
  }
);