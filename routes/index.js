/*
  /routes/index.js
*/
const express = require('express');
const router = express.Router();
const path = require('path');

// Importem controladors
var ctrlDir = path.resolve("App/Controllers/");
// Importem controlador de user
var userCtrl = require(path.join(ctrlDir, "User"));


//Middleware para mostrar datos del request
router.use(function (req, res, next) {
    console.log('/' + req.method);
    next();
});

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/', function (req, res) {
    res.sendFile(path.resolve('views/index.html'));
});


router.post("/mayor", userCtrl.isMayor);

//Link routes and functions
router.post('/user', userCtrl.add);
router.get('/users', userCtrl.list);
router.get('/user/:id', userCtrl.find);

module.exports = router;
